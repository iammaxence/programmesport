
var tab=new Array(); //Tableau qui comprend les jours de la semaine séléctionnés

function addExercice(day){
	var div;
	var table;
	if(document.getElementById("nom").value != "" ){ //rajouter condition pour séries,reps, repos, ordre

		if(tab[day]==undefined || tab[day]==null){ // Si le jour de la semaine selectionné n'es pas dans le tableau, je crée une div

			tab[day]=day; //Ajout du jour selectionné dans le tableau
			div=document.createElement("div");
			div.setAttribute("id",day); //id de la div=jour séléctionné
			div.appendChild(document.createTextNode(document.getElementById("jour").value)); //Affichage du jour de la semaine
			div.appendChild(document.createElement("br")); //saut de ligne
			document.getElementById("usefull").appendChild(div); //Ajout à la div usefull
			table=document.createElement("table"); //Creation d'un tableau pour chaque jour de la semaine
			table.setAttribute("id","table"+day); //exemple: id=tableLundi

			addTitleColumn(div,table);

		}
		else{ //Si le jour de la semaine a déjà été selectionné, on récupère l'id des balises pour ajouter du contenu au tableau
			div=document.getElementById(day);
			table=document.getElementById("table"+day);
		}

		//Correspond à l'affichage de chacune des entrés des input sous forme d'un table
		var tr=document.createElement("tr");
		var tdNom=document.createElement("td");
		tdNom.appendChild(document.createTextNode(document.getElementById("nom").value));
		var tdSeries=document.createElement("td");
		tdSeries.appendChild(document.createTextNode(document.getElementById("series").value));
		var tdReps=document.createElement("td");
		tdReps.appendChild(document.createTextNode(document.getElementById("reps").value));
		var tdRepos=document.createElement("td");
		tdRepos.appendChild(document.createTextNode(document.getElementById("repos").value));
		var tdOrdre=document.createElement("td");
		tdOrdre.appendChild(document.createTextNode(document.getElementById("ordre").value));

		//bouton suppression a ajouter à dernière colonnes de chaque ligne du tableau
		const img=document.createElement("img");
		img.setAttribute("src","delete-button.png");
		img.setAttribute("height","30");
		img.setAttribute("width","30");
		//fonctionne mais dans "inspecter" ça laisse parfois un vide blanc entre les balises
		img.addEventListener("click",function(){table.removeChild(tr); if (table.childNodes.length<2){tab[div.getAttribute("id")]=null;document.getElementById("usefull").removeChild(div);}}); //Retire des fils de la balise <tr>, l'élément clické		

		//Ajout des td dans la balise tr
		tr.appendChild(tdNom);
		tr.appendChild(tdSeries);
		tr.appendChild(tdReps);
		tr.appendChild(tdRepos);
		tr.appendChild(tdOrdre);
		tr.appendChild(img);
		table.appendChild(tr);
		//div.appendChild(table);	
		
	}

}

function addTitleColumn(div,table){
	var tr= document.createElement("tr");
	var thNom=document.createElement("th");
	thNom.appendChild(document.createTextNode("Nom"));
	var thSeries=document.createElement("th");
	thSeries.appendChild(document.createTextNode("Series"));
	var thReps=document.createElement("th");
	thReps.appendChild(document.createTextNode("Reps"));
	var thRepos=document.createElement("th");
	thRepos.appendChild(document.createTextNode("Repos"));
	var thOrdre=document.createElement("th");
	thOrdre.appendChild(document.createTextNode("Ordre"));


	div.appendChild(table);	

	tr.appendChild(thNom);
	tr.appendChild(thSeries);
	tr.appendChild(thReps);
	tr.appendChild(thRepos);
	tr.appendChild(thOrdre);

	table.appendChild(tr);
}
